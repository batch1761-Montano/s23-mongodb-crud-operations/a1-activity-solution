// Create 

// Add at leaast 5 of the members of your favorite fictional group or musical band into our users collection with the following fields:

// - firstname - String

// - lasName - String

// - email - String

// - password - String

// isAdmin - Boolean (initially all courses should be inactive)

db.users.insertMany([

	{

		"firstName": "Chito",

		"lastName": "Miranda",

        "email": "chitomiranda@mail.com",

		"password": "chito1234",

		"isAdmin": false

	},

        {

		"firstName": "Franco",

		"lastName": "Reyes",

        "email": "francoreyes@mail.com",

		"password": "franco1234",

		"isAdmin": false

	},

        {

		"firstName": "Rico",

		"lastName": "Blanco",

        "email": "ricoblanco@mail.com",

		"password": "rico1234",

		"isAdmin": false

	},

        {

		"firstName": "Andrew",

		"lastName": "E.",

        "email": "andrewe@mail.com",

		"password": "andrew1234",

		"isAdmin": false

	},

        {

		"firstName": "Francis",

		"lastName": "Magalona",

        "email": "francismagalona@mail.com",

		"password": "francis1234",

		"isAdmin": false

	},

])



// Add 3 new courses in a new courses collection with the following fields:

// - name - String

// - price - Number(

// - isActive - Boolean (initially all courses should be active.)

        

db.courses.insertMany([

        {

            "name": "HTML101",

            "price": 3000,

            "isActive": false

        },

        {

            "name": "CSS101",

            "price": 5000,

            "isActive": false

        },

        {

            "name": "Javascript101",

            "price": 7000,

            "isActive": false

        },

        

])

// Read

// Find all regular/non-admin users.

        

db.users.find()        

db.users.find({"isAdmin": false})  

   

// Update 

// Update the first user in the users collection as an admin.

db.users.updateOne({},{$set:{"isAdmin": true}})   

// Update one of the courses as active.

db.courses.updateOne({},{$set:{"isActive": true}})



// Delete all the inactive courses

db.courses.deleteMany({isActive:false})



        

